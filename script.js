const crashRide = document.getElementById("crash-ride");
const hiHatTop = document.getElementById("hihat-top");
const playingClass = 'playing';
window.addEventListener('keydown', playSound);

function animateCrashOrRide() {
	crashRide.style.transform = `rotate(0deg) scale(1.5)`;
}

function animateHiHatClosed() {
	hiHatTop.style.top = "171px";
}

function playSound(e) {
	// console.log(e.keyCode)
	const keyCode = e.keyCode;
	const keyElement = document.querySelector(`div[data-key= "${keyCode}"]`);
		// console.log(keyElement)
	if (!keyElement) return;
	const audioElement = document.querySelector(`audio[data-key= "${keyCode}"]`);
	// console.log(audioElement);
	audioElement.currentTime = 0;
	audioElement.play();
	switch (keyCode) {
		case 69:
		case 82:
			animateCrashOrRide();
			break;
		case 75:
			animateHiHatClosed();
			break;
	}
	keyElement.classList.add(playingClass);
}

function removeCrashRideTransition() {
	crashRide.style.transform = "rotate(-7deg) scale(1.5)";
}
crashRide.addEventListener('transitionend', removeCrashRideTransition);

function removeHiHatTopTransition() {
	hiHatTop.style.top = "166px";
}
hiHatTop.addEventListener('transitionend', removeHiHatTopTransition);

function removeKeyTransition(e) {
	e.target.classList.remove(playingClass);
}
const drumKeys = document.querySelectorAll(".key");
drumKeys.forEach(key => key.addEventListener('transitionend', removeKeyTransition));